## Objectif pour l'apprenant
- Ecriture de requêtes simples et complexes avec le langage SQL
- Ecriture de procédures stockées et triggers
- Conception et utilisation de transactions SQL.

##  Modalités

- Collaboration en binômes demandée.
- La définition des groupes doit se faire le premier jour.

##  Gestion de la communication

- Lisez les objectifs du client puis réalisez un point avec votre collègue afin de définir vos objectifs d'avancement.
- En cas d’incompréhension, centralisez vos questions techniques. Les réponses seront assurées et rédigées pour le sprint suivant.

## Présentation du projet

###  Objet du document

Ce document s'adresse à une équipe de développeurs en troisième année de formation

###  Descriptions des parties prenantes

- Client / Chef de projet : Formateur
- Développeur : Etudiant ·e ·s en 3e année

###  Objectifs

La solution _i-garden_ est une application numérique fictive permettant de gérer des plantes.

Cette application, destinée aux particuliers novices ou non dans le jardinage permet à son utilisateur de pouvoir gérer ses arrosages, entretiens, récoltes ou encore des tâches manuelles.

_Votre objectif, sera d'en modéliser la base de données ainsi que d'en générer certaines automatisations._

Ce module n’a pas pour objectif de vous demander d’implémenter une interface graphique.

#### Décomposition en sprints

Ce sujet est décomposé en plusieurs sprints. Chaque sprint doit être la suite logique de l’étape précédente. Ainsi, la base de données livrée à la fin d’un sprint, doit être modifiée par le biais de `ALTER` et de transactions les sprints suivants.

#### Sprint 01

**Les graines et les plantes**

 Une plante est définie par beaucoup de paramètres.
 Elle a des paramètres qui sont propres à sa variété de graine et qui ne changent pas:

- Nom*
- Nom latin
- Famille / Genre / Variété
- Classification* (Vivace, Annuelle ou Bisannuelle)
- Hauteur maximale
- Photo
- Icone
- Description

Historiquement, l'homme a essayé de classer les plantes au sein de "familles". Pour simplifier nous allons conserver quatre niveaux de profondeur au niveau de cette classification:

Famille > Genre > Espèce > Variété

Il n’est pas toujours possible de préciser la variété, certaines fois il y a des "trous" dans l'arborescence et il doit être possible d’associer une graine à tout niveau de cette arborescence.

Par exemple :
 - la `Tomate Beefsteak` est une variété de tomate, appartenant à l'espèce `tomate`. L'espèce tomate appartient au genre `Solanum` ainsi qu'à la famille des `Solanacées`.
 - La Pomme de terre `Vitelotte noire` est une variété de pomme de terre, appartenant au 
genre `Solanum` de la famille des `Solanacées`.
 - La Salade `Iceberg` est une variété de laitue, nous ne connaissons pas son genre mais elle appartient à la famille des `Asteracées`.

D'autres propriétés sont propres à la plante vivante elle-même telles que :

- la date du semis
- la date du repiquage
- la date de floraison
- la date de récolte
- la quantité récoltée

**Les utilisateurs**

Les utilisateurs seront définis par plusieurs critères. Dans un premier temps, nous allons simplement leurs attribuer des attributs de base :

- Pseudo*
- Email*
- MotDePasse*

Des utilisateurs peuvent posséder des jardins et un jardin peut être partagé entre plusieurs utilisateurs.

Au sein de la base de données, une plante n'aura pas forcément de jardin affecté. (Elle pourra simplement être déclarée dans le domaine public). Mais appartiendra à un jardin OU un utilisateur (l'un ou l'autre pas les deux). 

**Les états**

 Une plante peut être affectée par des états.  Elle peut par exemple être assoiffée et malade.

 Les états ont :

- un nom*
- une date (où l’état s’est déclenché)
- un statut* (Actif*, Passé, Traité)

**Les cycles de vie**

La plante aura, au cours de sa vie, différents cycles végétatifs. Ces derniers étant :
- germination
- croissance
- floraison
- fructification
- mort

##### Vues : 
Votre base de données devra contenir les vues suivantes (adaptez le nommage des vues aux appelations de vos tables):

- « Liste_des_plantes » contenant : Le nom de la plante, le nom de son propriétaire si défini, son cycle végétatif actuel, ses états actuels (Tous les états Actifs, combinés en une seule colonne, espacés par des virgules)

- « Plantes_hors_zone » : liste des plantes qui ne sont affectées à aucun jardin :
	- nom  de la plante
	- propriétaire

- « Plantes_recap » Liste des graines :
	- Nom
	- Son espèce  et le parent de son espèce (Genre ou Famille en fonction des cas)
	- Le parent de l'espèce doit commencer par une majuscule, tout le reste écrit en minuscule, l'ensemble au sein d'une seule colonne
	- Nom latin

##### Contraintes :

- Le changement de statut d’un état d’une plante doit être historisé.
- Une indexation doit être faite sur le nom de la classification des familles.
- Le changement de cycle végétatif doit être historisé.
- Une plante peut changer de propriétaire, en quel cas ce changement doit être historisé
- La suppression d’une plante doit supprimer les historiques associés


##### Livrables attendus à la fin de ce premier sprint

- **Modéliser le MEA de cette base de données**
- **Modéliser le MPD de cette base de données**
- **Produire le fichier SQL permettant de générer cette base de données tout en respectant les contraintes**.

- **Ce fichier SQL devra contenir également les vues** 

  
#### FAQ

Des questions ont pu être posées lors des réalisations précédentes de cet exercice, en voici les réponses :

- A quoi correspondent les astérisques dans le sujet
	- A préciser les champs obligatoires des tables

- En quoi consiste l'historisation ?
	- Il doit être possible de voir l'historique du changement d'une manière chronologique. Cette historique doit être généré automatiquement par la base de données


  

#### Sprint 02

Si vous n’avez pas terminé le premier sprint lors de la session précédente, finalisez le premier sprint en implémentant les dernières fonctionnalités tout en respectant les contraintes de migration stipulées en début du sprint 02 (Avec des ALTER, LOCK et des transactions si nécessaire.)

Après avoir modélisé la première version, votre client vous demande d'en améliorer la structure avec le besoin suivant. Réalisez une évolution des schémas précédents ainsi qu'un script de migration. Cette migration doit s’assurer que nulle modification n’est appliquée sur la base de données lors de la migration. Elle ne doit également pas faire perdre de données utilisateur.

###### Les zones

Il a été omis au sein du premier sprint de préciser que les jardins doivent avoir une latitude et une longitude. Ces deux coordonnées sont écrites sous la forme décimale. La précision de ces coordonnées doit être de 6 chiffres après la virgule (~10 cm). Un jardin possède également une largeur et une hauteur, ces dimensions seront stockées en centimètres (entier >= 0). Ces informations additionnelles pourront être nulles.

##### Les notes

L'utilisateur peut vouloir à certains moments rajouter des Notes sur un jardin, une plante ou encore sur une graine .

 Ces Notes auront les attributs suivants :
- type (texte, audio)*,
- description,
- lien audio
- date de creation.

 Le lien audio sera  obligatoire si le type audio est choisi. La description sera obligatoire si le type texte est choisi.


##### Compte Demo au sein de la base de données.

Un utilisateur « Demo » doit être défini au sein de la base de données. Ce dernier ne doit pouvoir réaliser que de la consultation de la base de données. Que ce soit pour les tables mais aussi pour les vues.

##### Evènement

Certains bots arrivent à créer des comptes automatiquement. Réaliser une procédure stockée associée à un évènement qui supprime automatiquement, toutes les nuits, les comptes utilisateurs n'ayant aucun jardin ni plante associée. 


##### Contraintes :
- Le script de migration ne doit pas faire perdre de données, ainsi les plantes existantes ne doivent pas perdre leurs noms définis.
- Lorsqu'on supprime le compte d'un utilisateur l'ensemble des plantes qui lui sont associées sont supprimées ainsi que les notes émises. S’il était seul détenteur d’un jardin, le jardin doit également être supprimé.
- La suppression d’une plante implique la suppression des notes associées.
- La suppression d’un jardin implique la suppression des plantes et notes associées.

##### Objectifs de ce second sprint

Scripts SQL de migration (faites attention à ce que vos vues, l’utilisateur demo et vos scripts soient inclus !)
Ce script ne doit pas impacter les utilisateurs actuels ni faire perdre de la data. Implémentez le en réalisant des ALTER, TRANSACTION et LOCK.

###  Conseils

- Réfléchissez et concertez-vous avant d’avancer sur le sujet.
- Le contenu que vous intégrerez devra être travaillé, soigné et professionnel.
- Privilégiez la qualité de la solution plutôt que de couvrir l’intégralité du sujet.